import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CopangWeb from '~/components/copang-web'
Vue.component('CopangWeb', CopangWeb)
